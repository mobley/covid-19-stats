
const acronymArray = [
    ['SH', 'Schleswig-Holstein'],
    ['HH', 'Hamburg'],
    ['NI', 'Niedersachen'],
    ['HB', 'Bremen'],
    ['NW', 'Nordrhein-Westfalen'],
    ['HE', 'Hessen'],
    ['RP', 'Rheinland-Pfalz'],
    ['BW', 'Baden-Württemberg'],
    ['BY', 'Bayern'],
    ['SL', 'Saarland'],
    ['BB', 'Brandenburg'],
    ['MV', 'Meckenburg-Vorpommern'],
    ['SN', 'Sachsen'],
    ['ST', 'Sachsen-Anhalt'],
    ['TH', 'Thüringen'],
    ['BE', 'Berlin']
]
const ACRONYM_MAP = new Map(acronymArray);

async function getData() {
    const response = await (await fetch('/getdata')).text();

    return response;
}

function makeArrayFromData(data) {
    allDataArray = [];

    const lines = data.split('\n');

    for (const head of lines[0].split(',')) {
        const newhead = ACRONYM_MAP.get(head.slice(3, 5)) ?? head;
        allDataArray.push([newhead]);
    }

    for (let i = 1; i < lines.length; i++) {
        let splittedLine = lines[i].split(',');

        for (let i = 0; i < splittedLine.length; i++) {
            allDataArray[i].push(splittedLine[i]);
        }
    }

    return allDataArray;
}



window.onload = async function () {
    // load data into javascipt
    const data = await getData();

    allDataArray = makeArrayFromData(data);
    allDataArray.pop(); // delete sum

    const timestamps = allDataArray[0].slice(2).map(x => x.slice(0, 10));
    timestamps.unshift('x');

    allDataArray.shift(); // delete timestamps

    document.querySelector('#checkboxes').innerHTML = createMarkupForCheckboxes();

    var chart = c3.generate({
        bindto: '#chart',
        data: {
            columns: [
                timestamps,
            ],
            type: 'spline',
            x: 'x'
        },
        point: {
            show: false,
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: '%Y-%m-%d'
                },
                label: 'Datum'
            },
            y: {
                // max: 50000, // set value max of y
                min: 0,
                label: 'Anzahl'
            }
        },



    });
    console.log([allDataArray[1]])
    chart.load({
        columns: [
            timestamps,
        ],
        x: 'x'
    });

    checkboxesElem = document.querySelector('#checkboxes');
    checkboxesElem.addEventListener("click", handleUIEvent);
    function handleUIEvent(event) {
        if (event.target.type === 'checkbox') {
            console.log(event.target.id);
            toggleStatus(event.target.id);
        }
    }
    let stateStatus = {};
    acronymArray.map(x => stateStatus[x[1]] = false);

    let stateArrayPos = new Map();

    allDataArray.map((x, index) => stateArrayPos.set(x[0], index));

    console.log(stateArrayPos);

    function toggleStatus(id) {
        if (stateStatus[id]) {
            chart.unload({ ids: id });
            stateStatus[id] = false;
        } else {
            chart.load({
                columns: [
                    allDataArray[stateArrayPos.get(id)],
                    timestamps
                ]
            });
            stateStatus[id] = true;
        }
    }
    document.querySelector('#save-as-png').onclick = function () {
        let chartSVG = document.getElementById("chart").firstChild;

        // needed to fix style of output
        Array.from(chartSVG.querySelectorAll('.c3-chart-line')).map(x => x.style.fill = "none");
        Array.from(chartSVG.querySelectorAll('.c3-axis')).map(x => x.style.fill = "none");
        Array.from(chartSVG.querySelectorAll('.domain, .tick line')).map(x => x.style.stroke = "black");
        Array.from(chartSVG.querySelectorAll('.tick')).map(x => x.style.fill = "black");

        chartSVG.querySelector('.c3-axis-x-label').style.fill = "black";
        chartSVG.querySelector('.c3-axis-y-label').style.fill = "black";
        chartSVG.style.font = "10px sans-serif";

        saveSvg(chartSVG, "diagram.svg");

    }
}


function createMarkupForCheckboxes() {
    let rowMarkup = `
<div class="row">
{{~it.someArray :value:index}}
<div class="column">
    <input type="checkbox" id="{{=value}}">
    <label class="label-inline" for="{{=value}}">{{=value}}</label>
</div>
{{~}}
</div>
`;

    let rows = `
{{~it.rows :value:index}}
{{=value}}
{{~}}`;

    let rowTemplate = doT.template(rowMarkup);
    let grid = doT.template(rows);

    const states = Array.from(ACRONYM_MAP.values());
    let columnArray = [];
    for (let i = 0; i < 4; i++) {
        columnArray.push(rowTemplate({ someArray: states.slice(i * 4, i * 4 + 4) }))
    }

    const gridMarkup = grid({ rows: columnArray });
    return gridMarkup;
}

// found on https://stackoverflow.com/a/46403589/13704678
function saveSvg(svgEl, name) {
    svgEl.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    var svgData = svgEl.outerHTML;

    var preface = '<?xml version="1.0" standalone="no"?>\r\n';
    var svgBlob = new Blob([preface, svgData], { type: "image/svg+xml;charset=utf-8" });
    var svgUrl = URL.createObjectURL(svgBlob);
    var downloadLink = document.createElement("a");
    downloadLink.href = svgUrl;
    downloadLink.download = name;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}
