import cherrypy
import requests
import pathlib

csv_url = 'https://raw.githubusercontent.com/jgehrcke/covid-19-germany-gae/master/cases-rki-by-state.csv'

def download_file(url: str, path: pathlib.Path):
    # Daten runterladen
    try:
        r = requests.get(url)
    except Exception as e:
        print("Datei konnte nicht runtergeladen werden. Fehlermeldung: ", e)
        exit()

    # Datei speichern
    try:
        with open(path, 'wb') as f:
            f.write(r.content)
    except Exception as e:
        print("Speichern der Datei schlug mit folgender Fehlermeldung fehl: ", e)
        exit()



class Application():
    @cherrypy.expose
    def index(self):
        return open('./frontend/index.html', encoding='utf-8')

    @cherrypy.expose
    def getdata(self):
        return open('data.csv')

if __name__ == '__main__':
    current_directory = pathlib.Path.cwd()

    file_path = pathlib.Path('data.csv')

    download_file(csv_url, file_path) 
    print("Download der Datei erfolgreich abgeschlossen.")

    conf = {
	
    '/': {
        'tools.staticdir.root': current_directory,
        'tools.staticdir.on': True,
        'tools.staticdir.dir': "./frontend"}}

    cherrypy.tree.mount(Application(), "/", conf)

    cherrypy.config.update({'server.socket_port': 8000})
    cherrypy.config.update({'log.screen': False})

    print("Server wird gestartet.")
    cherrypy.engine.start()
    print("Server läuft...")
    cherrypy.engine.block()


