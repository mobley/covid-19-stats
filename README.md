![](screenshot.png)

# Installation
`pip install -r requirements.txt`

# Benutzung
`python main.py`

Benutzeroberfläche ist dann erreichbar unter: `localhost:8000`

Beim starten des Servers wird die neuste Version des Datensatzes heruntergeladen.
